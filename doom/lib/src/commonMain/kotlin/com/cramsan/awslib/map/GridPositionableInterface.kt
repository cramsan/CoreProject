package com.cramsan.awslib.map

/**
 * This interface defines a contract for elements that need to be placed in the [GameMap].
 */
interface GridPositionableInterface {
    var posX: Int
    var posY: Int
}
