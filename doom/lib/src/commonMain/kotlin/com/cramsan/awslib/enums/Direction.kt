package com.cramsan.awslib.enums

/**
 * Represents the possible directions a GameEntity can be facing.
 * [KEEP] represents no change in the direction.
 */
enum class Direction {
    NORTH, SOUTH, WEST, EAST, KEEP
}
