package com.cramsan.awslib.enums

/**
 * This Enum represents the possible actions that a Character can take.
 */
enum class TurnActionType {
    MOVE, ATTACK, NONE
}
