package com.cramsan.awslib.entity.implementation

/**
 * Define the type of an equipable item
 */
enum class EquippableType {
    KNIFE,
    HANDGUN,
    INVALID
}
