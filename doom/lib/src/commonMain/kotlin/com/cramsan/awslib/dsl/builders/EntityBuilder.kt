package com.cramsan.awslib.dsl.builders

/**
 * Class that will build a [GameEntity]
 */
abstract class EntityBuilder(
    val templateName: String,
)
