package com.cramsan.awslib.entity.implementation

/**
 * Define the type of ally
 */
enum class AllyType {
    CIVILIAN,
    SCIENTIST,
    MARINE,
    INVALID
}
