package com.cramsan.awslib.entity.implementation

/**
 * Define the type of enemy
 */
enum class EnemyType {
    DOG,
    SOLDIER,
    IMP,
    INVALID
}
