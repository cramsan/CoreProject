package com.cramsan.awslib.entity.implementation

/**
 * Define the type of a placeable entity
 */
enum class PlaceableType {
    COMPUTER,
    CRATE,
    INVALID
}
