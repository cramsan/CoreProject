package com.cramsan.awslib.utils.constants

object TerrainIds {
    const val OPEN = 0
    const val WALL = 1
    const val DOOR = 2
    const val END = 3
}
