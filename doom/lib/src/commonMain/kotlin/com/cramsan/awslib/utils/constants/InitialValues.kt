package com.cramsan.awslib.utils.constants

object InitialValues {

    const val INVALID_ID = "UNDEFINED"
    const val INVALID_AMOUNT = Int.MIN_VALUE
    const val NOOP_ID = "NOOP"
    const val PLAYER_ID = "player"

    const val ENEMY_GROUP = "1"
    const val POS_X_ENTITY = 0
    const val POS_Y_ENTITY = 0
    const val PRIORITY_ENTITY = 10
    const val ENABLED_ENTITY = true
    const val PLACEABLE_HEALTH = 10

    const val HEALTH_PLAYER = 10
    const val GROUP_PLAYER = "0"
    const val CHARACTER_ATTACK = 0
    const val ATTACK_DOCTOR = 1
    const val ATTACK_DOG = 5

    const val SPEED_PLAYER = 10

    const val POS_X_TRIGGER = 0
    const val POS_Y_TRIGGER = 0
    const val ENABLED_TRIGGER = false

    const val INVALID_LABEL_OPTION = ""

    const val INVALID_TEXT_EVENT = ""
}
