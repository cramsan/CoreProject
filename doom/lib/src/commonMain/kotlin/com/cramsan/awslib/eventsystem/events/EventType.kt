package com.cramsan.awslib.eventsystem.events

enum class EventType {
    INTERACTION,
    CHANGETRIGGER,
    SWAPIDENTITY,
    NOOP
}
