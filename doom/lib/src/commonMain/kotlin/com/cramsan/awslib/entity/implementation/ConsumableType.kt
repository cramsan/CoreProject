package com.cramsan.awslib.entity.implementation

/**
 * Define the type of consumable
 */
enum class ConsumableType {
    HEALTH,
    ARMOR,
    CREDIT,
    INVALID
}
