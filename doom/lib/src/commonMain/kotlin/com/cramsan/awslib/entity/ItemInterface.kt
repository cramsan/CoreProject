package com.cramsan.awslib.entity

/**
 * This interface defines a contract for elements that appear in the [com.cramsan.awslib.map.GameMap] and that
 * can be picked up.
 */
interface ItemInterface : GameEntityInterface
