package com.cramsan.awsgame.renderer

data class Point(val x: Double, val y: Double)
