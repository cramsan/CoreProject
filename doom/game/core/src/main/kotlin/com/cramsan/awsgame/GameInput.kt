package com.cramsan.awsgame

enum class GameInput {
    UP, DOWN, LEFT, RIGHT, NONE, ACTION
}
