package com.cramsan.awsgame.renderer

data class Step(
    val x: Double,
    val y: Double,
    var height: Double,
    var distance: Double,
    val length: Double,
    var shading: Double,
    var offset: Double
)
