package com.cramsan.awsgame.renderer

data class Projection(val top: Double, val height: Double)
