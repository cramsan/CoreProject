# Doom 
This is the source code for the Doom project. A game that tries to replicate the [Doom RPG](https://doom.fandom.com/wiki/Doom_RPG) experience.

# Build Status

Android: [![Build Status](https://dev.azure.com/CRamsan/Doom-Project/_apis/build/status/Doom-Project_Android?branchName=master)](https://dev.azure.com/CRamsan/Doom-Project/_build/latest?definitionId=11&branchName=master)

Desktop: [![Build Status](https://dev.azure.com/CRamsan/Doom-Project/_apis/build/status/Doom-Project_Desktop?branchName=master)](https://dev.azure.com/CRamsan/Doom-Project/_build/latest?definitionId=12&branchName=master)

# Analytics

**Engagement** (*Not configured*)

**Issues** (*Not configured*)

**Events** (*Not configured*)

**Play Stores** (*Not configured*)

# Bug Tracker

[Github Issues](https://github.com/CRamsan/PetProject/labels/doom)

# Distribution
**Internal** (*Not configured*)

**Play Store** (*Not configured*)

# Feedback

**Reviews** (*Not configured*)

**Ratings** (*Not configured*)

## References
---

- Template: [PROJECT.md](../docs/templates/PROJECT.md).