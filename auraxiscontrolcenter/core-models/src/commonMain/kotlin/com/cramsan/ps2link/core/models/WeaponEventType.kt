package com.cramsan.ps2link.core.models

/**
 * @Author cramsan
 * @created 2/2/2021
 */
enum class WeaponEventType {
    KILLS,
    HEADSHOT_KILLS,
    VEHICLE_KILLS,
    KILLED_BY,
    DAMAGE_GIVEN,
    DAMAGE_TAKEN,
}
