package com.cramsan.ps2link.core.models

data class Rank(
    val name: String?,
    val ordinal: Long?,
)
