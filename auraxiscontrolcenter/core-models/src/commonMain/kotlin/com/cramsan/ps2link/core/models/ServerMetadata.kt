package com.cramsan.ps2link.core.models

/**
 * @Author cramsan
 * @created 1/30/2021
 */
data class ServerMetadata(
    val status: ServerStatus,
    val population: Population,
)
