package com.cramsan.ps2link.core.models

/**
 * @Author cramsan
 * @created 1/16/2021
 */
enum class KillType {
    KILL,
    KILLEDBY,
    SUICIDE,
    UNKNOWN,
}
