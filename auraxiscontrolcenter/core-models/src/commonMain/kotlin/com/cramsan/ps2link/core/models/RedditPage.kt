package com.cramsan.ps2link.core.models

enum class RedditPage(val path: String) {
    PLANETSIDE("r/Planetside"),
    PS4PLANETSIDE2("r/PS4Planetside2"),
}
