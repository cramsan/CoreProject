package com.cramsan.ps2link.core.models

/**
 * @Author cramsan
 * @created 1/26/2021
 */
data class WeaponStatItem(
    val stats: Map<Faction, Long?>
)
