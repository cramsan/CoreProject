package com.cramsan.ps2link.core.models

/**
 * @Author cramsan
 * @created 1/22/2021
 */
enum class ServerStatus {
    ONLINE,
    OFFLINE,
    LOCKED,
    UNKNOWN;
}
