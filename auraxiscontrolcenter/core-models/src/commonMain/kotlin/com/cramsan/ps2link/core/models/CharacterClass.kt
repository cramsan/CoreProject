package com.cramsan.ps2link.core.models

enum class CharacterClass {
    LIGHT_ASSAULT,
    ENGINEER,
    MEDIC,
    INFILTRATOR,
    HEAVY_ASSAULT,
    MAX,
    UNKNOWN,
}
