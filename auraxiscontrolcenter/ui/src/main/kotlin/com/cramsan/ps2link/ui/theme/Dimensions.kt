package com.cramsan.ps2link.ui.theme

import androidx.compose.ui.unit.dp

object Size {
    val xmicro = 1.dp
    val micro = 2.dp
    val xsmall = 3.dp
    val small = 6.dp
    val medium = 12.dp
    val large = 24.dp
    val xlarge = 36.dp
    val xxlarge = 48.dp
}

object Padding {
    val micro = 1.dp
    val xsmall = 2.dp
    val small = 4.dp
    val medium = 8.dp
    val large = 16.dp
    val xlarge = 24.dp
    val xxlarge = 36.dp
}

val MaxDialogSize = 250.dp

object Opacity {
    const val transparent = 0.10f
    const val translucent = 0.55f
}
