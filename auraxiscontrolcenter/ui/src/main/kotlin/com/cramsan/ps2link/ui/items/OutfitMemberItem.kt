package com.cramsan.ps2link.ui.items

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.cramsan.ps2link.core.models.LoginStatus
import com.cramsan.ps2link.ui.SlimButton
import com.cramsan.ps2link.ui.theme.PS2Theme
import com.cramsan.ps2link.ui.theme.Padding
import com.cramsan.ps2link.ui.toColor
import com.cramsan.ps2link.ui.toStringResource

@Composable
fun OutfitMemberItem(
    modifier: Modifier = Modifier,
    label: String,
    outfitRank: String,
    loginStatus: LoginStatus,
    onClick: () -> Unit = {},
) {
    SlimButton(
        onClick = onClick,
        modifier = modifier
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Text(
                text = label,
                textAlign = TextAlign.Start,
            )
            Spacer(modifier = Modifier.weight(1f))
            Text(
                modifier = Modifier.padding(horizontal = Padding.small),
                text = outfitRank,
                style = MaterialTheme.typography.caption,
            )
            Text(
                text = loginStatus.toStringResource(),
                color = loginStatus.toColor()
            )
        }
    }
}

@Preview
@Composable
fun OutfitMemberItemPreview() {
    PS2Theme {
        OutfitMemberItem(
            label = "Cramsan",
            outfitRank = "Member",
            loginStatus = LoginStatus.ONLINE,
        )
    }
}
