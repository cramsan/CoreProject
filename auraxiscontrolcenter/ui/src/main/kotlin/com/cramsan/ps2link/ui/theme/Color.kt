package com.cramsan.ps2link.ui.theme

import androidx.compose.ui.graphics.Color

/**
 * Primary color palette
 */
val primaryColor = Color(0xFF006064)
val primaryLightColor = Color(0xFF428e92)
val primaryDarkColor = Color(0xFF00363a)
val secondaryColor = Color(0xFF26c6da)
val secondaryLightColor = Color(0xFF6ff9ff)
val secondaryDarkColor = Color(0xFF0095a8)
val primaryTextColor = Color(0xFFFFFFFF)
val secondaryTextColor = Color(0xFF000000)
val errorColor = Color(0xFFFF0000)

/**
 * BattleRank colors
 */
val gold = Color(0xFFFFE606)
val goldWhite = Color(0xFFFFF7B1)
val goldDisabled = Color(0xFF69611B)
val goldBackground = Color(0xFF2C290D)

/**
 * Cert Colors
 */
val certOrange = Color(0xFFFF5722)
val certWhite = Color(0xFFFF7D5A)
val certBackground = Color(0xFF381B12)

/**
 * UI Colors
 */
val negative = Color(0xFFFF0000)
val positive = Color(0xFF00FF0A)
val warning = Color(0xFFFFC107)
val undefined = Color(0xFFB9B9B9)
