import com.cramsan.ps2link.db.models.CharacterClass;
import com.cramsan.ps2link.db.models.Faction;
import com.cramsan.ps2link.db.models.LoginStatus;
import com.cramsan.ps2link.db.models.Namespace;
import kotlin.time.Duration;
import kotlinx.datetime.Instant;

CREATE TABLE Character (
    id TEXT NOT NULL PRIMARY KEY UNIQUE,
    name TEXT,
    activeProfileId INTEGER AS CharacterClass,
    loginStatus TEXT AS LoginStatus NOT NULL,
    currentPoints INTEGER,
    percentageToNextCert REAL,
    percentageToNextRank REAL,
    rank INTEGER,
    outfitRank INTEGER,
    lastLogin INTEGER AS Instant,
    minutesPlayed INTEGER AS Duration,
    factionId TEXT AS Faction NOT NULL,
    worldId TEXT,
    worldName TEXT,
    outfitId TEXT,
    outfitName TEXT,
    namespace TEXT AS Namespace NOT NULL,
    cached INTEGER AS Boolean NOT NULL,
    lastUpdated INTEGER AS Instant NOT NULL
);

getCharacter:
SELECT * FROM Character WHERE id == ? AND namespace == ?;

getAllCharacters:
SELECT * FROM Character WHERE cached == 1;

insertCharacter:
INSERT OR REPLACE INTO Character(
    id,
    name,
    activeProfileId,
    loginStatus,
    currentPoints,
    percentageToNextCert,
    percentageToNextRank,
    rank,
    outfitRank,
    lastLogin,
    minutesPlayed,
    factionId,
    worldId,
    worldName,
    outfitId,
    outfitName,
    namespace,
    cached,
    lastUpdated
 ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);

insertCharacterInstance:
INSERT OR REPLACE INTO Character(
    id,
    name,
    activeProfileId,
    loginStatus,
    currentPoints,
    percentageToNextCert,
    percentageToNextRank,
    rank,
    outfitRank,
    lastLogin,
    minutesPlayed,
    factionId,
    worldId,
    worldName,
    outfitId,
    outfitName,
    namespace,
    cached,
    lastUpdated
 ) VALUES ?;

deleteCharacter:
DELETE FROM Character WHERE id == ? AND namespace == ?;

deleteAll:
DELETE FROM Character;