package com.cramsan.appcore.twitter

object TwitterModuleConstants {
    const val CONSUMER_SECRET = "CONSUMER_SECRET"
    const val CONSUMER_KEY = "CONSUMER_KEY"
    const val ACCESS_TOKEN = "ACCESS_TOKEN"
    const val ACCESS_TOKEN_SECRET = "ACCESS_TOKEN_SECRET"
}
