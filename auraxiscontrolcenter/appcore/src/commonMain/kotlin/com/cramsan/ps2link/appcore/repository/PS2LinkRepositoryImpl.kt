package com.cramsan.ps2link.appcore.repository

import com.cramsan.ps2link.appcore.census.DBGServiceClient
import com.cramsan.ps2link.appcore.localizedName
import com.cramsan.ps2link.appcore.network.PS2HttpResponse
import com.cramsan.ps2link.appcore.network.onSuccess
import com.cramsan.ps2link.appcore.network.process
import com.cramsan.ps2link.appcore.network.processList
import com.cramsan.ps2link.appcore.network.requireBody
import com.cramsan.ps2link.appcore.network.toFailure
import com.cramsan.ps2link.appcore.sqldelight.DbgDAO
import com.cramsan.ps2link.appcore.toCoreModel
import com.cramsan.ps2link.core.models.CensusLang
import com.cramsan.ps2link.core.models.Character
import com.cramsan.ps2link.core.models.KillEvent
import com.cramsan.ps2link.core.models.LoginStatus
import com.cramsan.ps2link.core.models.Namespace
import com.cramsan.ps2link.core.models.Outfit
import com.cramsan.ps2link.core.models.Server
import com.cramsan.ps2link.core.models.ServerMetadata
import com.cramsan.ps2link.core.models.StatItem
import com.cramsan.ps2link.core.models.WeaponItem
import com.cramsan.ps2link.network.models.content.World
import com.cramsan.ps2link.network.models.content.response.server.PS2
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.datetime.Clock
import kotlin.time.ExperimentalTime
import kotlin.time.minutes

class PS2LinkRepositoryImpl(
    private val dbgCensus: DBGServiceClient,
    private val dbgDAO: DbgDAO,
    private val clock: Clock,
) : PS2LinkRepository {

    @OptIn(ExperimentalTime::class)
    override suspend fun saveCharacter(character: Character) {
        dbgDAO.insertCharacter(character)
    }

    override suspend fun removeCharacter(characterId: String, namespace: Namespace) {
        dbgDAO.removeCharacter(characterId, namespace)
    }

    override fun getAllCharactersAsFlow(): Flow<List<Character>> {
        return dbgDAO.getAllCharactersAsFlow()
    }

    override suspend fun getAllCharacters(): PS2HttpResponse<List<Character>> {
        return PS2HttpResponse.success(dbgDAO.getCharacters())
    }

    override suspend fun getCharacter(
        characterId: String,
        namespace: Namespace,
        lang: CensusLang,
        forceUpdate: Boolean,
    ): PS2HttpResponse<Character> {
        val cachedCharacter = dbgDAO.getCharacter(characterId, namespace)
        if (!forceUpdate && (cachedCharacter != null && isCharacterValid(cachedCharacter))) {
            return PS2HttpResponse.success(cachedCharacter)
        }
        val response = dbgCensus.getProfile(characterId, namespace, lang)
        response.onSuccess {
            dbgDAO.insertCharacter(it.copy(cached = cachedCharacter?.cached ?: false))
        }
        return response
    }

    override fun getCharacterAsFlow(
        characterId: String,
        namespace: Namespace,
    ): Flow<Character?> {
        return dbgDAO.getCharacterAsFlow(characterId, namespace)
    }

    override suspend fun searchForCharacter(
        searchField: String,
        currentLang: CensusLang,
    ): PS2HttpResponse<List<Character>> = coroutineScope {
        if (searchField.length < 3) {
            return@coroutineScope PS2HttpResponse.success(emptyList())
        }
        Namespace.validNamespaces.map { namespace ->
            val job = async {
                val endpointProfileList = dbgCensus.getProfiles(
                    searchField = searchField,
                    namespace = namespace,
                    currentLang = currentLang
                )
                endpointProfileList
            }
            job
        }.awaitAll().processList { it }.process { it.flatten() }
    }

    override suspend fun getFriendList(
        characterId: String,
        namespace: Namespace,
        lang: CensusLang,
    ): PS2HttpResponse<List<Character>> {
        return dbgCensus.getFriendList(
            character_id = characterId,
            namespace = namespace,
            currentLang = lang,
        )
    }

    override suspend fun getKillList(
        characterId: String,
        namespace: Namespace,
        lang: CensusLang,
    ): PS2HttpResponse<List<KillEvent>> {
        return dbgCensus.getKillList(
            character_id = characterId,
            namespace = namespace,
            currentLang = lang,
        )
    }

    override suspend fun getStatList(
        characterId: String,
        namespace: Namespace,
        currentLang: CensusLang,
    ): PS2HttpResponse<List<StatItem>> {
        return dbgCensus.getStatList(
            character_id = characterId,
            namespace = namespace,
            currentLang = currentLang,
        )
    }

    override suspend fun getWeaponList(
        characterId: String,
        namespace: Namespace,
        lang: CensusLang,
    ): PS2HttpResponse<List<WeaponItem>> {
        val response = getCharacter(characterId, namespace, lang)
        if (!response.isSuccessful) {
            return response.toFailure()
        }
        val character = response.requireBody()
        return dbgCensus.getWeaponList(characterId, character.faction, namespace, lang)
    }

    override suspend fun getServerList(lang: CensusLang): PS2HttpResponse<List<Server>> = coroutineScope {
        val serverPopulation = dbgCensus.getServerPopulation()
        if (!serverPopulation.isSuccessful) {
            return@coroutineScope serverPopulation.toFailure()
        }
        Namespace.validNamespaces.map { namespace ->
            val job = async {
                dbgCensus.getServerList(namespace, lang).process { list ->
                    list.mapNotNull { world ->
                        world.world_id?.let {
                            val serverMetadata =
                                getServerMetadata(world, serverPopulation.requireBody(), lang)
                            Server(
                                worldId = it,
                                serverName = world.name?.localizedName(lang) ?: "",
                                namespace = namespace,
                                serverMetadata = serverMetadata
                            )
                        }
                    }
                }
            }
            job
        }.awaitAll().processList { it }.process { it.flatten() }
    }

    override suspend fun saveOutfit(outfit: Outfit) {
        dbgDAO.insertOutfit(outfit)
    }

    override suspend fun removeOutfit(outfitId: String, namespace: Namespace) {
        dbgDAO.removeOutfit(outfitId, namespace)
    }

    override fun getAllOutfitsAsFlow(): Flow<List<Outfit>> {
        return dbgDAO.getAllOutfitsAsFlow()
    }

    override suspend fun getAllOutfits(): PS2HttpResponse<List<Outfit>> {
        return PS2HttpResponse.success(dbgDAO.getAllOutfits())
    }

    override suspend fun getOutfit(
        outfitId: String,
        namespace: Namespace,
        lang: CensusLang,
        forceUpdate: Boolean
    ): PS2HttpResponse<Outfit> {
        val cachedOutfit = dbgDAO.getOutfit(outfitId, namespace)
        if (!forceUpdate && (cachedOutfit != null && isOutfitValid(cachedOutfit))) {
            return PS2HttpResponse.success(cachedOutfit)
        }
        val response = dbgCensus.getOutfit(outfitId, namespace, lang)
        response.onSuccess {
            dbgDAO.insertOutfit(it.copy(cached = cachedOutfit?.cached ?: false))
        }
        return response
    }

    override fun getOutfitAsFlow(outfitId: String, namespace: Namespace): Flow<Outfit?> {
        return dbgDAO.getOutfitAsFlow(outfitId, namespace)
    }

    override suspend fun searchForOutfits(
        tagSearchField: String,
        nameSearchField: String,
        currentLang: CensusLang,
    ): PS2HttpResponse<List<Outfit>> = coroutineScope {
        if (tagSearchField.length < 3 && nameSearchField.length < 3) {
            return@coroutineScope PS2HttpResponse.success(emptyList())
        }

        Namespace.validNamespaces.map { namespace ->
            val job = async {
                val endpointOutfitList = dbgCensus.getOutfitList(
                    outfitTag = tagSearchField,
                    outfitName = nameSearchField,
                    namespace = namespace,
                    currentLang = currentLang
                )
                endpointOutfitList
            }
            job
        }.awaitAll().processList { it }.process { it.flatten() }
    }

    override suspend fun getMembersOnline(outfitId: String, namespace: Namespace, currentLang: CensusLang): PS2HttpResponse<List<Character>> {
        val response = dbgCensus.getMembersOnline(outfitId, namespace, currentLang)
        if (!response.isSuccessful) {
            return response.toFailure()
        }
        return response.process { members ->
            members.filter { it.loginStatus != LoginStatus.OFFLINE }
        }
    }

    override suspend fun getMembers(outfitId: String, namespace: Namespace, currentLang: CensusLang): PS2HttpResponse<List<Character>> {
        return dbgCensus.getMemberList(outfitId, namespace, currentLang)
    }

    @OptIn(ExperimentalTime::class)
    private fun isCharacterValid(character: Character?): Boolean {
        character?.lastUpdate.let {
            return if (it == null) {
                false
            } else {
                it + EXPIRATION_TIME < clock.now()
            }
        }
    }

    @OptIn(ExperimentalTime::class)
    private fun isOutfitValid(outfit: Outfit?): Boolean {
        outfit?.lastUpdate.let {
            return if (it == null) {
                false
            } else {
                it + EXPIRATION_TIME < clock.now()
            }
        }
    }

    companion object {
        @OptIn(ExperimentalTime::class)
        val EXPIRATION_TIME = 1.minutes
    }
}

private fun getServerMetadata(world: World, ps2Metadata: PS2?, currentLang: CensusLang): ServerMetadata {
    val server = when (world.name?.localizedName(currentLang)) {
        "Ceres" -> ps2Metadata?.livePS4?.ceres
        "Genudine" -> ps2Metadata?.livePS4?.genudine
        "Cobalt" -> ps2Metadata?.live?.cobalt
        "Connery" -> ps2Metadata?.live?.connery
        "Emerald" -> ps2Metadata?.live?.emerald
        "Miller" -> ps2Metadata?.live?.miller
        else -> null
    }

    val population = server?.status.toCoreModel()
    val status = world.state.toCoreModel()

    return ServerMetadata(
        status = status,
        population = population,
    )
}
