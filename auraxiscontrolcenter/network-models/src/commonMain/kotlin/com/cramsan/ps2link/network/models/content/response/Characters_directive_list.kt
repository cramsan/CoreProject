package com.cramsan.ps2link.network.models.content.response

import com.cramsan.ps2link.network.models.content.CharacterDirective

data class Characters_directive_list(
    val characters_directive_list: List<CharacterDirective>? = null
)
