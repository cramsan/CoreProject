package com.cramsan.ps2link.network.models.content

data class CharacterDirectiveObjective(

    val character_id: String? = null,
    val directive_id: String? = null,
    val objective_group_id: String? = null,
    val objective_id: String? = null,
    val state_data: String? = null,
    val status: String? = null,
)
