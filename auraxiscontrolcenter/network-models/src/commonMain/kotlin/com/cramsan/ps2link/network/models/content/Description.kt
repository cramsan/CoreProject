package com.cramsan.ps2link.network.models.content

import kotlinx.serialization.Serializable

@Serializable
data class Description(

    /**
     * @return The en
     */
    /**
     * @param en The en
     */
    val en: String? = null
)
