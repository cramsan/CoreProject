package com.cramsan.ps2link.network.models.content.response

import com.cramsan.ps2link.network.models.content.DirectiveTier

data class Directive_tier_list(
    val directive_tier_list: List<DirectiveTier>? = null
)
