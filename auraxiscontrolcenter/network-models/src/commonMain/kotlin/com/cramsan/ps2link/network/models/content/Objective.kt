package com.cramsan.ps2link.network.models.content

data class Objective(
    val objective_group_id: String? = null,
    val objective_id: String? = null,
    val objective_type_id: String? = null,
    val param1: String? = null,
    val param2: String? = null,
)
