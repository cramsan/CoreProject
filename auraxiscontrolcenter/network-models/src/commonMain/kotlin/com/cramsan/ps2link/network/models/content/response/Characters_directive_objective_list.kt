package com.cramsan.ps2link.network.models.content.response

import com.cramsan.ps2link.network.models.content.CharacterDirectiveObjective

data class Characters_directive_objective_list(
    val characters_directive_objective_list: List<CharacterDirectiveObjective>? = null
)
