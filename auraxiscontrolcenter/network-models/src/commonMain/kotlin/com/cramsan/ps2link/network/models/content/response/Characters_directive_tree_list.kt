package com.cramsan.ps2link.network.models.content.response

import com.cramsan.ps2link.network.models.content.CharacterDirectiveTree

data class Characters_directive_tree_list(
    val characters_directive_tree_list: List<CharacterDirectiveTree>? = null
)
