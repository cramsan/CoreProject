package com.cramsan.ps2link.network.models.content.response

import com.cramsan.ps2link.network.models.content.CharacterDirectiveTier

data class Characters_directive_tier_list(
    val characters_directive_tier_list: List<CharacterDirectiveTier>? = null
)
