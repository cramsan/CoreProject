package com.cramsan.ps2link.network.models.content.response

import com.cramsan.ps2link.network.models.content.DirectiveTreeCategory

data class Directive_tree_category_list(
    val directive_tree_category_list: List<DirectiveTreeCategory>? = null
)
