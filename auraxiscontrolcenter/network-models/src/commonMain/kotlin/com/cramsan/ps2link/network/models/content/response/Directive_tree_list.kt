package com.cramsan.ps2link.network.models.content.response

import com.cramsan.ps2link.network.models.content.CharacterDirectiveTree

data class Directive_tree_list(
    val directive_tree_list: List<CharacterDirectiveTree>? = null
)
