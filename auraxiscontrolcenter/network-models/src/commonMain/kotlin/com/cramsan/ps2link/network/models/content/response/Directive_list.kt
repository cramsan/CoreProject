package com.cramsan.ps2link.network.models.content.response

import com.cramsan.ps2link.network.models.content.Directive

data class Directive_list(
    val directive_list: List<Directive>? = null
)
