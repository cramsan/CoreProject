# Web Service

## Objective
To have a service that can provide an API so that the app can always have up-to-date information of the plant catalog.

## Platform
This web service runs using the Spring framework.