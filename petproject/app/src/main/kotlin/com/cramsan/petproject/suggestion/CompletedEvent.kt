package com.cramsan.petproject.suggestion

import com.cramsan.petproject.base.BaseEvent

class CompletedEvent(val suggestionSubmitted: Boolean) : BaseEvent()
