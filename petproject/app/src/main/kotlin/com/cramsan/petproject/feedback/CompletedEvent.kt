package com.cramsan.petproject.feedback

import com.cramsan.petproject.base.BaseEvent

class CompletedEvent(val feedbackSubmitted: Boolean) : BaseEvent()
