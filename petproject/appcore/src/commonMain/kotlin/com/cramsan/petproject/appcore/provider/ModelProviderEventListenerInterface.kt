package com.cramsan.petproject.appcore.provider

interface ModelProviderEventListenerInterface {

    fun onCatalogUpdate(isReady: Boolean)
}
