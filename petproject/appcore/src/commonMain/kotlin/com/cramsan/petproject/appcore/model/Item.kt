package com.cramsan.petproject.appcore.model

open class Item(
    val id: Int,
    val exactName: String,
    val commonNames: String,
    val imageUrl: String
)
