package com.cramsan.petproject.appcore.model

enum class AnimalType {
    CAT,
    DOG,
    ALL
}
