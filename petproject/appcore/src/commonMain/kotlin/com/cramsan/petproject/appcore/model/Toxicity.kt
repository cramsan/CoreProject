package com.cramsan.petproject.appcore.model

class Toxicity(
    val isToxic: ToxicityValue,
    val source: String
)
