package com.cramsan.petproject.appcore.model

enum class ToxicityValue {
    TOXIC,
    NON_TOXIC,
    UNDETERMINED
}
