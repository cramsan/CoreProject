# AppCore

## Objective
To have business logic that is written in a cross-platform way and organized in modules that can be consumed independetly. This project does not provide UI or presentation logic.

## Platform
This project use [Kotlin Multiplatform](https://kotlinlang.org/docs/reference/multiplatform.html) to target JVM, Android and iOS.