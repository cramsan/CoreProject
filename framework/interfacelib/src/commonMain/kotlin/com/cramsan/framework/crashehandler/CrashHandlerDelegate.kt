package com.cramsan.framework.crashehandler

/**
 * Implementation of the delegate to handle crashes
 */
interface CrashHandlerDelegate {

    /**
     * Initialize this delegate
     */
    fun initialize()
}
