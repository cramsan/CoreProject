package com.cramsan.framework.logging

/**
 * Severity used for logging purposes
 */
enum class Severity {
    VERBOSE,
    DEBUG,
    INFO,
    WARNING,
    ERROR
}
